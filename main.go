package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"text/template"
	"time"

	"github.com/gorilla/mux"
)

var index_tpl = `
<!DOCTYPE html>
<html>

<head>
<style>
.bodyrow:hover {
  background-color: rgb(240, 240, 240); 
}
.center {
  margin-left: auto;
  margin-right: auto;
}
table {
  border-collapse:collapse;
}
tr.next {
  padding: 2px;
  border: solid 2px;
}

</style>
</head>

<body>

<table class="center">
<thead>
<tr>
<th style="text-align:right;">Trash Day</td>
<th style="text-align:center;">|</td>
<th style="text-align:left;">Apartment</td>
</tr>
</thead>

<tbody>
{{ range .WeekList }}
<tr style="color:rgba(0,0,0,{{.Alpha}})" class="{{ if .IsNext }}next{{ end }} bodyrow">
  <td style="text-align:right;" style="padding: 10px;">{{ .OutDate }}</td>
  <td style="text-align:center;">|</td>
  <td style="text-align:left;">{{ .Apartment }}</td>
</tr>
{{ end }}
</tbody>

</table>

{{ if .TrashDay }}
<p style="text-align:center;">( {{.TrashDay}} is trash day! )</p>
{{else}}
<p style="text-align:center;">( trash goes to curb on {{ .Weeknight }} :) ) </p>
{{ end }}



</body>

</html>
`

// <p style="text-align:center;"> {{ .OutDate }} | {{ .Apartment }}</p>
// <p style="color:rgba(0,0,0,{{ .Opacity }});"> {{ .Date }} | {{ .Apartment }}</p>
type WeekAssignment struct {
	OutDate   string
	Apartment string
	Alpha     string
	IsNext    bool
}

var (
	// order          = []string{"Monica and Chris", "Eli", "John", "Mercy And Will"}
	order = []string{"Mercy And Will", "Eli and Amira", "John"}
	// reference_date = time.Date(2023, 7, 5, 0, 0, 0, 0, time.Local)
	reference_date = time.Date(2024, 12, 11, 0, 0, 0, 0, time.Local)
)

// var trash_day = time.Wednesday
const TRASH_DAY = time.Wednesday

func GetNextTrashDay(d time.Time, trash_day time.Weekday) time.Time {
	for i := 0; i < 7; i++ {
		t := d.Add(time.Duration(24*i) * time.Hour)
		day := t.Weekday()

		if time.Weekday(day) == TRASH_DAY {
			return t
		}
	}

	return d
}

func NegMod(a, n int) int {
	return a - n*int(math.Floor(float64(a)/float64(n)))
}

func CalculateApartment(trash_day time.Time) int {
	n_weeks := math.Floor(trash_day.Sub(reference_date).Hours() / (24 * 7))
	apartment := NegMod(int(n_weeks), len(order))
	return apartment
}

func GetNextEightWeeks() []WeekAssignment {
	today := time.Now()

	week_assignments := make([]WeekAssignment, 0)

	for i := 0; i < 8; i++ {
		offset := today.Add(time.Duration(i*7*24) * time.Hour)

		apartment := CalculateApartment(offset)

		week := WeekAssignment{
			OutDate:   GetNextTrashDay(offset, TRASH_DAY).Format("Jan 02 2006 (Mon)"),
			Apartment: order[apartment],
			Alpha:     fmt.Sprintf("%0.1f", 1.0),
		}

		if i == 0 {
			week.IsNext = true
		}

		week_assignments = append(week_assignments, week)
	}

	return week_assignments
}

func GetNineWeeks() []WeekAssignment {
	next_trash_day := GetNextTrashDay(time.Now(), TRASH_DAY)

	w := []WeekAssignment{}
	for i := -4; i < 5; i++ {

		curr_trash_day := next_trash_day.Add(time.Duration(i*7*24) * time.Hour)

		apartment := CalculateApartment(curr_trash_day)

		alpha := 0.5 - 0.1*math.Abs(float64(i))
		if i == 0 {
			alpha = 1.0
		}

		week := WeekAssignment{
			OutDate:   curr_trash_day.Format("Jan 02 2006 (Mon)"),
			Apartment: order[apartment],
			Alpha:     fmt.Sprintf("%0.1f", alpha),
		}

		if i == 0 {
			week.IsNext = true
		}

		w = append(w, week)
	}

	return w
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	tpl, err := template.New("index").Parse(index_tpl)
	if err != nil {
		log.Fatal("template failed to parse:", err)
	}

	is_trash_day := ""

	// test_time := time.Date(2023, 7, 4, 0, 0, 0, 0, time.Local)

	if time.Now().Weekday() == TRASH_DAY {
		// if test_time.Weekday() == TRASH_DAY {
		is_trash_day = "Today"
	}

	if time.Now().Weekday() == TRASH_DAY-1 {
		// if test_time.Weekday() == (TRASH_DAY - 1) {
		is_trash_day = "Tomorrow"
	}

	data := struct {
		WeekList  []WeekAssignment
		Weeknight string
		TrashDay  string
	}{
		// WeekList:  GetNextEightWeeks(),
		WeekList:  GetNineWeeks(),
		Weeknight: time.Weekday(int(TRASH_DAY) - 1).String(),
		TrashDay:  is_trash_day,
	}

	err = tpl.Execute(w, data)
	if err != nil {
		log.Fatal("template failed to render: ", err)
	}
}

// Not actually deploying this, as I know how hackers think and
// this would be perceived as a challeng... but it would be cathartic
//
//func EnvHandler(w http.ResponseWriter, r *http.Request) {
//	_, err := w.Write([]byte("Eat my butthole"))
//	if err != nil {
//		fmt.Println("Failed to tell someone to fuck off")
//	}
//}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", IndexHandler)
	// r.HandleFunc("/.env", EnvHandler)

	addr := ":8080"
	fmt.Println("starting server on " + addr)
	http.ListenAndServe(addr, r)
}
